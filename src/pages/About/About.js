import styles from "./About.module.css";

const About = () => {
  return (
    <div className={styles.about}>
      <h2>
        Atividade Somativa 2 <span>Mini Blog</span>
      </h2>
      <p>
        Este projeto consiste em um blog feito com React no front-end e Firebase
        no back-end para a Atividade Somativa 2.
      </p>
    </div>
  );
};

export default About;