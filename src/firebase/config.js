
import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyCQzzOSC9XbrVbrwtvjRPYOnR5ZGdOGj8Q",
  authDomain: "as2-miniblog.firebaseapp.com",
  projectId: "as2-miniblog",
  storageBucket: "as2-miniblog.appspot.com",
  messagingSenderId: "809438727522",
  appId: "1:809438727522:web:f93853bf9e99501b25f03b"
};


const app = initializeApp(firebaseConfig);

const db = getFirestore(app);

export { db };
