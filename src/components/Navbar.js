import { NavLink } from "react-router-dom";

import { useAuthentication } from "../pages/hooks/useAuthentication";

import { UseAuthValue } from "../context/AuthContext";

import styles from "./Navbar.module.css";

const Navbar = () => {

  const {user} = UseAuthValue();
  const {logout} = useAuthentication();

  return(  <nav className={styles.navbar}>
    <NavLink to="/" className={styles.brand}>
        Mini <span>Blog</span>
    </NavLink>
    <ul className={styles.links_list}>
        <li>
            <NavLink to="/" className={({isActive}) => (isActive ? styles.active : "")} >Home</NavLink>
        </li>
        {!user && (
          <>
          <li>
              <NavLink
                to="/login"
                className={({ isActive }) => (isActive ? styles.active : "")}
              >
                Entrar
              </NavLink>
            </li>
            <li>
              <NavLink
                to="/register"
                className={({ isActive }) => (isActive ? styles.active : "")}
              >
                Cadastrar
              </NavLink>
            </li>
          </>
        )}
            <li>
        <NavLink to="/about">About</NavLink>
        </li>
        {user && (
          <li>
            <button onClick={logout}>Logout</button>
          </li>
        )}
    </ul>
  </nav>);
}

export default Navbar